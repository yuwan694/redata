#!/usr/bin/env python

from redata.common_dot_com import CommonDotCom


class DomainDotCom(CommonDotCom):
    def _build_suburb_url(self, a_dict):
        my_url = '{}-{}-{:04d}'.format(a_dict['locality'], a_dict['state'], a_dict['postcode'])
        my_url = my_url.replace(' ', '-')
        my_url = my_url.replace('\'', '-')
        self.the_suburb_url = my_url.lower()

    def sold_listings(self):
        my_url = 'https://www.domain.com.au/sold-listings/{}/'.format(self.the_suburb_url)
        my_params = {}
        my_params.update(self.the_kwargs)
        my_top_path = ['listingsMap']
        my_info_locator = {
            'url': ['listingModel', 'url'],
            'price': ['listingModel', 'price'],
            'agent': ['listingModel', 'branding', 'agentNames'],
            'brand': ['listingModel', 'branding', 'brandName'],
            'street': ['listingModel', 'address', 'street'],
            'suburb': ['listingModel', 'address', 'suburb'],
            'state': ['listingModel', 'address', 'state'],
            'postcode': ['listingModel', 'address', 'postcode'],
            'beds': ['listingModel', 'features', 'beds'],
            'baths': ['listingModel', 'features', 'baths'],
            'parking': ['listingModel', 'features', 'parking'],
            'propertyType': ['listingModel', 'features', 'propertyType'],
            'landSize': ['listingModel', 'features', 'landSize'],
            'date': ['listingModel', 'tags', 'tagText'],
        }

        self._clear_data()
        for my_page in range(self.the_begin_page, self.the_end_page + 1):
            self._report_page_progress(my_page)
            my_params.update({'page': str(my_page)})
            my_json = self._read_json_data(my_url,
                                           my_params,
                                           my_page,
                                           'window[\'__domain_group/APP_PROPS\']',
                                           '</script>')
            my_items = self._browse_dict_nodes(my_json, my_top_path)
            if my_items:
                self._store_data([], my_items, my_info_locator)
            else:
                if self.the_verbose:
                    print('Items {} is missing from page [{}], Stop.'.format(my_top_path, my_page))
                break
        return self.the_data


if __name__ == '__main__':
    my_enable_all = True

    if None or my_enable_all:
        try:
            my_invalid = DomainDotCom(suburb='noon')
        except ValueError as e:
            print('Exception(ValueError):', e)

        try:
            my_invalid = DomainDotCom(suburb='noon+2022+nsw')
        except ValueError as e:
            print('Exception(ValueError):', e)

        try:
            my_invalid = DomainDotCom(suburb='-2022')
        except ValueError as e:
            print('Exception(ValueError):', e)

        my_domain = DomainDotCom(suburb='cherrybrook+2126+nsw', begin_page=1, end_page=51)
        my_data = my_domain.sold_listings()
        for my_item in my_data:
            print('item:', my_item, my_data[my_item])

        my_df = my_domain.df
        my_purified_df = my_domain.get_purified_df(oldest_date='2000-01-02', highest_price='10000000')

    if True or my_enable_all:
        my_domain = DomainDotCom(suburb='glenwood+2768+nsw', begin_page=1, end_page=51, purge_cache=False)
        my_data = my_domain.sold_listings()
        for my_item in my_data:
            print('item:', my_item, my_data[my_item])
