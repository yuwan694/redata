#!/usr/bin/env python

import getpass
import os


class RedataUtils(object):
    the_user_name = getpass.getuser()
    the_cache_dir = '~/.cache/redata-{}/' if True else '/tmp/redata-{}/'
    the_unicode_encoding = 'utf-8'

    def __init__(self, **kwargs):
        self.the_verbose = True

        self.the_kwargs = {}
        self.update_config(**kwargs)

    def update_config(self, **kwargs):
        self.the_kwargs.update(kwargs)
        self.the_verbose = kwargs.get('verbose', self.the_verbose)

    def _make_hashable(self, an_input):
        my_hashable = an_input
        if isinstance(an_input, str):
            my_hashable = an_input.encode(self.the_unicode_encoding)
        else:
            try:
                my_hashable = str(an_input).encode(self.the_unicode_encoding)
            except TypeError as e:
                if self.the_verbose:
                    print('Exception:', e)
        if None and self.the_verbose:
            print('RU_make_hashable:', my_hashable)
        return my_hashable

    def update_hash(self, a_hash, an_input):
        a_hash.update(self._make_hashable(an_input))

    def user_cache_dir(self):
        return os.path.expanduser(self.the_cache_dir.format(self.the_user_name))


if __name__ == '__main__':
    print(RedataUtils().user_cache_dir())
