#!/usr/bin/env python

from redata.common_dot_com import CommonDotCom


class RealestateViewDotCom(CommonDotCom):
    def _build_suburb_url(self, a_dict):
        my_url = '{}|{}|{:04d}'.format(a_dict['locality'], a_dict['state'], a_dict['postcode'])
        self.the_suburb_url = my_url.lower()

    def sold_listings(self):
        my_url = 'https://www.realestateview.com.au/portal/search/'
        my_params = {
            'smt': 'Sold',
            'loc': self.the_suburb_url,
            'iss': 'True',
            'ao': 'False',
            'ofi': 'False',
            'amd': '01/01/0001 00:00:00',
            'sort': '',
            'page': '1',
        }
        my_params.update(self.the_kwargs)
        my_top_path = ['Items']
        my_info_locator = {
            'url': ['Urls', 'FullDetailsUrl'],
            'price': ['Price', 'Price'],
            'agent': ['Agencies', 0, 'Agents', 0, 'Name'],
            'brand': ['Agencies', 0, 'Name'],
            'street': ['Address', 'StreetAddress'],
            'suburb': ['Address', 'Suburb'],
            'state': ['Address', 'State'],
            'postcode': ['Address', 'Postcode'],
            'beds': ['NumBedrooms'],
            'baths': ['NumBathrooms'],
            'parking': ['TotalParking'],
            'propertyType': ['PropertyTypes'],
            'landSize': ['ListingEnquiryForm', 'Listing', 'PropertyData', 'PropertySize', 'LandArea'],
            'date': ['Status', 'SoldDateDisplayText'],
        }

        self._clear_data()
        for my_page in range(self.the_begin_page, self.the_end_page + 1):
            self._report_page_progress(my_page)
            my_params.update({'page': '{}'.format(my_page)})
            my_json = self._read_json_data(my_url,
                                           my_params,
                                           my_page,
                                           'var data =',
                                           'var model =')
            my_items = self._browse_dict_nodes(my_json, my_top_path)
            self._store_data(['Id'], my_items, my_info_locator)
            if len(my_items) < 1:
                if self.the_verbose:
                    print('The list is empty from page [{}], Stop.'.format(my_page))
                break
        return self.the_data


if __name__ == '__main__':
    my_enable_all = True

    if None or my_enable_all:
        try:
            my_invalid = RealestateViewDotCom(suburb='non')
        except ValueError as e:
            print('Exception(ValueError):', e)
        try:
            my_invalid = RealestateViewDotCom(suburb='200')
        except ValueError as e:
            print('Exception(ValueError):', e)

        my_real = RealestateViewDotCom(suburb='north strathfield+2137+nsw', begin_page=1)
        my_data = my_real.sold_listings()
        for my_item in my_data:
            print('item:', my_item, my_data[my_item])
        print('Count:', len(my_data))

        my_df = my_real.df
        my_purified_df = my_real.get_purified_df(oldest_date='2000-01-02', highest_price='10000000')

    if True or my_enable_all:
        my_real = RealestateViewDotCom(suburb='glenwood+2768+nsw', begin_page=1, purge_cache=False)
        my_data = my_real.sold_listings()
        for my_item in my_data:
            print('item:', my_item, my_data[my_item])
        print('Count:', len(my_data))
