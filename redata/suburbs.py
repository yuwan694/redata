#!/usr/bin/env python

import os


class AussieSuburbs(object):
    the_current_directory = os.path.dirname(os.path.realpath(__file__))
    the_dictionary_path = the_current_directory + '/suburbs.dict'
    the_dictionary = eval(open(the_dictionary_path).read())

    def __init__(self, **kwargs):
        self.the_verbose = True

        self.the_kwargs = {}
        self.update_config(**kwargs)

    def update_config(self, **kwargs):
        self.the_kwargs.update(kwargs)
        self.the_verbose = self.the_kwargs.get('verbose', self.the_verbose)

    def search(self, a_search):
        return {my_key.lower(): self.the_dictionary[my_key]
                for my_key in self.the_dictionary
                if a_search.upper() in my_key}


if __name__ == '__main__':
    my_suburbs = AussieSuburbs()

    my_ret = my_suburbs.search('noon')
    print('Found {} entries:', len(my_ret))
    for my_entry in my_ret:
        print('\t', my_entry)

    my_ret = my_suburbs.search('strathfield')
    print('Found {} entries:', len(my_ret))
    for my_entry in my_ret:
        print('\t', my_entry)

    my_ret = my_suburbs.search('O\'brien')
    print('Found {} entries:', len(my_ret))
    for my_entry in my_ret:
        print('\t', my_entry)

    my_ret = my_suburbs.search('3551')
    print('Found {} entries:', len(my_ret))
    for my_entry in my_ret:
        print('\t', my_entry)

    my_ret = my_suburbs.search('strathfield south+2136+nsw')
    print('Found {} entries:', len(my_ret))
    for my_entry in my_ret:
        print('\t', my_entry)

    my_ret = my_suburbs.search('non-exist-at-all')
    print('Found {} entries:', len(my_ret))
    for my_entry in my_ret:
        print('\t', my_entry)
