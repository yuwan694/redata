#!/usr/bin/env python

import re
import datetime
import json
from pandas import DataFrame
from redata.redata_cache import RedataCache
from redata.suburbs import AussieSuburbs


class CommonDotCom(object):
    the_min_price = 1000

    def __init__(self, **kwargs):
        self.the_suburb_url = ''  # per site URL form
        self.the_suburb_dict = {}  # AussieSuburbs value
        self.the_suburb = ''  # AussieSuburbs key (in lower case), should be validate
        self.the_suburb_suggests = []  # suggests from AussieSuburbs
        self.the_begin_page = 1
        self.the_end_page = 200
        self.the_encoding = 'utf-8'
        self.the_verbose = True
        self.the_highest_price = 0
        self.the_oldest_date = ''
        self.the_aso = None
        self.the_page_processed = 0
        self.the_data = {}

        self.the_kwargs = {}
        self.update_config(**kwargs)

    def update_config(self, **kwargs):
        self.the_kwargs.update(kwargs)
        self.the_suburb = self.the_kwargs.get('suburb', self.the_suburb)
        self.the_begin_page = self.the_kwargs.get('begin_page', self.the_begin_page)
        self.the_end_page = self.the_kwargs.get('end_page', self.the_end_page)
        self.the_encoding = self.the_kwargs.get('encoding', self.the_encoding)
        self.the_verbose = self.the_kwargs.get('verbose', self.the_verbose)
        self.the_highest_price = int(self.the_kwargs.get('highest_price', self.the_highest_price))
        self.the_oldest_date = self.the_kwargs.get('oldest_date', self.the_oldest_date)
        self.the_aso = AussieSuburbs(**self.the_kwargs)

        if self._check_suburb():
            self._build_suburb_url(self.the_suburb_dict)
        else:
            if len(self.the_suburb_suggests) > 0:
                print('Try these suburbs:', self.the_suburb_suggests)
            else:
                print('No suggest for suburb [{}], try other suburbs.'.format(self.the_suburb))
            raise ValueError('invalid suburb [{}]'.format(self.the_suburb))

    def _build_suburb_url(self, a_dict):
        self.the_suburb_url = '@invalidate@' + str(a_dict)
        raise NotImplemented('This will be implemented in derived classes.')

    def _check_suburb(self):
        if self.the_verbose and None:
            print('_check_suburb [{}].'.format(self.the_suburb))

        my_candidates = self.the_aso.search(self.the_suburb)
        if len(my_candidates) == 1:
            self.the_suburb_dict = next(iter(my_candidates.values()))
            return True

        self.the_suburb_suggests = my_candidates.keys()
        return False

    def _clear_data(self):
        self.the_data = {}
        self.the_page_processed = 0

    def _transform(self, a_item):
        self._transform_price(a_item)
        self._transform_date(a_item)
        self._transform_beds(a_item)
        self._transform_baths(a_item)
        self._transform_parking(a_item)
        self._transform_land_size(a_item)

    def _transform_beds(self, a_item):
        if 'beds' in a_item and a_item['beds']:
            try:
                a_item['beds'] = int(a_item['beds'])
            except ValueError as e:
                if self.the_verbose:
                    print('Exception(ValueError):', e)
                a_item['beds'] = float('nan')

    def _transform_baths(self, a_item):
        if 'baths' in a_item and a_item['baths']:
            try:
                a_item['baths'] = int(a_item['baths'])
            except ValueError as e:
                if self.the_verbose:
                    print('Exception(ValueError):', e)
                a_item['baths'] = float('nan')

    def _transform_parking(self, a_item):
        if 'parking' in a_item and a_item['parking']:
            try:
                a_item['parking'] = int(a_item['parking'])
            except ValueError as e:
                if self.the_verbose:
                    print('Exception(ValueError):', e)
                a_item['parking'] = float('nan')

    def _transform_land_size(self, a_item):
        if 'landSize' in a_item and a_item['landSize']:
            try:
                a_item['landSize'] = float(a_item['landSize'])
            except ValueError as e:
                if self.the_verbose:
                    print('Exception(ValueError):', e)
                a_item['parking'] = float('nan')

    def _transform_price(self, a_item):
        if 'price' in a_item and a_item['price']:
            try:
                a_item['price'] = a_item['price'].replace(',', '')
                a_item['price'] = a_item['price'].replace('$', '')
                a_item['price'] = int(a_item['price'].replace('$', ''))
            except ValueError as e:
                if self.the_verbose and None:
                    print('Exception(ValueError):', e)
                a_item['price'] = float('nan')

    def _transform_date(self, a_item):
        if 'date' in a_item and a_item['date']:
            try:
                my_date = a_item['date']
                my_date = ' '.join(my_date.split()[-3:])
                my_date_words = my_date.split()
                if len(my_date_words) == 3:
                    a_item['date'] = datetime.datetime.strptime(my_date, '%d %b %Y')
                elif len(my_date_words) == 2:
                    a_item['date'] = datetime.datetime.strptime(my_date, '%b %Y')
                else:
                    a_item['date'] = datetime.datetime.today()
            except ValueError as e:
                if self.the_verbose and None:
                    print('Exception(ValueError):', e)
                a_item['date'] = datetime.datetime.today()

    def _browse_dict_nodes(self, a_dict, a_path):
        if type(a_path) is not list:
            print('(Error): a_path should be a list.')
            return None

        for my_node in a_path:
            if my_node is None:
                if self.the_verbose and None:
                    print('Any None entry in a_path will result in None.')
                return None

        my_info = a_dict
        my_node = None
        try:
            for my_node in a_path:
                my_info = my_info[my_node]
        except KeyError as e:
            if self.the_verbose and None:
                print('Exception(KeyError):', e, ' | node [{}] is missing in'.format(my_node), a_path)
            return None
        except TypeError as e:
            if self.the_verbose and None:
                print('Exception(TypeError):', e, ' | node [{}] is missing in'.format(my_node), a_path)
            return None
        except IndexError as e:
            if self.the_verbose and None:
                print('Exception(IndexError):', e, ' | node [{}] is missing in'.format(my_node), a_path)
            return None
        return my_info

    def _store_data(self, an_id_path, a_items, an_info_locator):
        if self.the_verbose and None:
            print('Storing item keys:', a_items.keys())

        if type(a_items) == dict:
            my_range = a_items.keys()
        elif type(a_items) == list:
            my_range = [x for x in range(len(a_items))]
        else:
            print('(Error): a_items should be a list or dict.')
            return None

        for my_item in my_range:
            if type(a_items) == list:
                my_data_key = self._browse_dict_nodes(a_items[my_item], an_id_path)
            else:
                my_data_key = my_item
            self.the_data[my_data_key] = {}
            if self.the_verbose and None:
                print('Processing item:', a_items[my_item])
            for my_key in sorted(an_info_locator.keys()):
                my_info = self._browse_dict_nodes(a_items[my_item], an_info_locator[my_key])
                if self.the_verbose and None:
                    print('Item {} info: {} = {}'.format(my_data_key, my_key, my_info))
                self.the_data[my_data_key][my_key] = my_info
            self._transform(self.the_data[my_data_key])

    def _report_page_progress(self, a_page):
        self.the_page_processed += 1
        if self.the_verbose:
            print('page+{:02d}, '.format(a_page), end='', flush=True)
            if self.the_page_processed % 10 == 0:
                print()

    def _read_json_data(self, an_url, a_params, a_page, an_unique_begin, an_unique_end):
        my_params = {}
        my_params.update(a_params)
        my_params.update(self.the_kwargs)
        my_content = RedataCache(an_url, **a_params).data.decode(self.the_encoding)

        my_begin = my_content.find(an_unique_begin)
        my_begin = my_content.find('{', my_begin)
        my_end = my_content.find(an_unique_end, my_begin)
        my_end = my_content.rfind('}', my_begin, my_end) + 1
        my_extracted = my_content[my_begin:my_end]
        if self.the_verbose and None:
            print('URL: {}'.format(an_url))
            print('Params: {}'.format(a_params))
            print('Content Length: {}'.format(len(my_content)))
            if None:
                print('Content: {}'.format(my_content))
            print('Unique Begin: {}'.format(an_unique_begin))
            print('Unique End: {}'.format(an_unique_end))
            print('Contents extracted: begin={}, end={},'.format(my_begin, my_end),
                  my_extracted[:50], '...', my_extracted[-50:])
        if None:
            with open('/tmp/page{}.json'.format(a_page), 'wb') as f:
                f.write(my_extracted.encode('utf-8'))

        my_json = json.loads(my_extracted)
        if self.the_verbose and None:
            print('Json keys():', my_json.keys())
        return my_json

    @property
    def data(self):
        return self.the_data

    @property
    def df(self):
        return DataFrame.from_dict(self.the_data, orient='index')

    def _purify_price(self, a_df):
        if self.the_highest_price:
            my_cols = (self.the_min_price < a_df['price']) & (a_df['price'] < self.the_highest_price)
            if len(a_df[~my_cols].index) > 0:
                print('The deleted entries by _purify_price is', a_df[~my_cols].shape,
                      ': Excluded values are', a_df[~my_cols]['price'].unique(),
                      ': NaN entries', a_df[a_df['price'].isnull()].shape,)
            return a_df[my_cols]
        return a_df

    def _purify_date(self, a_df):
        if self.the_oldest_date:
            try:
                my_date = datetime.datetime.strptime(self.the_oldest_date, '%Y-%m-%d')
            except ValueError as e:
                print('Exception(ValueError):', e)
                print('(Error) The oldest_date keyword argument is not in the form of \'2018-10-14\', try again.')
                return a_df
            my_cols = a_df['date'] > my_date
            if len(a_df[~my_cols].index) > 0:
                print('The deleted entries by _purify_date is', a_df[~my_cols].shape,
                      ': Excluded values are', a_df[~my_cols]['date'].unique())
            return a_df[my_cols]
        return a_df

    def _purify_suburb(self, a_df):
        if self.the_suburb_dict:
            my_dict = self.the_suburb_dict
            my_ref = my_dict['locality'] + '+' + my_dict['state'] + '+' + '{:04d}'.format(my_dict['postcode'])
            my_cols = (a_df['suburb'].apply(str.upper)
                       + '+' + a_df['state'].apply(str.upper)
                       + '+' + a_df['postcode'] == my_ref)
            if len(a_df[~my_cols].index) > 0:
                print('The deleted entries by _purify_suburb is', a_df[~my_cols].shape,
                      ': Excluded values are', a_df[~my_cols]['suburb'].unique())
            return a_df[my_cols]
        return a_df

    def get_purified_df(self, **kwargs):
        self.update_config(**kwargs)
        my_df = self.df
        my_df = self._purify_price(my_df)
        my_df = self._purify_date(my_df)
        my_df = self._purify_suburb(my_df)
        my_df = my_df.set_index('date')
        my_df = my_df.sort_index()
        return my_df


if __name__ == '__main__':
    my_match = re.search('[0-9]+', 'north+strathfield,+nsw+-2137,l')
    if my_match:
        my_postcode = my_match.group(0)
        print(my_postcode)
