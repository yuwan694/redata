#!/usr/bin/env python

from diskcache import Cache
import hashlib
import time
import datetime
import requests
from redata.redata_utils import RedataUtils


class RedataCache(object):
    def __init__(self, a_url, **kwargs):
        self.the_url = a_url
        self.the_purge_cache = False
        self.the_verbose = True
        self.the_data = None
        self.the_ru = None

        self.the_kwargs = {}
        self.update_config(**kwargs)

    def update_config(self, **kwargs):
        self.the_kwargs.update(kwargs)
        self.the_ru = RedataUtils(**self.the_kwargs)
        self.the_purge_cache = self.the_kwargs.pop('purge_cache', self.the_purge_cache)
        self.the_verbose = self.the_kwargs.pop('verbose', self.the_verbose)

        self.the_kwargs.pop('begin_page', None)
        self.the_kwargs.pop('end_page', None)
        self.the_kwargs.pop('transaction', None)
        self.the_kwargs.pop('suburb', None)
        self.the_kwargs.pop('encoding', None)
        self.the_kwargs.pop('min_price', None)
        self.the_kwargs.pop('highest_price', None)
        self.the_kwargs.pop('oldest_date', None)

    @property
    def _cache_dir(self):
        return self.the_ru.user_cache_dir()

    def _cache_key(self):
        my_hash = hashlib.sha256()
        self.the_ru.update_hash(my_hash, self.the_url)
        for my_key in sorted(self.the_kwargs.keys()):
            self.the_ru.update_hash(my_hash, my_key)
            self.the_ru.update_hash(my_hash, self.the_kwargs[my_key])
        self.the_ru.update_hash(my_hash, datetime.date.today().strftime('%Y%m%d'))  # per day cache
        return my_hash.digest()

    def _cache_miss(self, a_cache, a_cache_key):
        my_http_headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) Chrome/51.0.2704.106'}
        my_request = requests.get(self.the_url, self.the_kwargs, headers=my_http_headers)
        a_cache[a_cache_key] = my_request.content

    def _reload(self):
        with Cache(self._cache_dir) as my_cache:
            my_time_point = time.time()
            my_cache_key = self._cache_key()
            if self.the_purge_cache and my_cache_key in my_cache:
                del my_cache[my_cache_key]
            if my_cache_key not in my_cache:
                self._cache_miss(my_cache, my_cache_key)
            self.the_data = my_cache[my_cache_key]
            self.the_seconds = time.time() - my_time_point

    @property
    def data(self):
        if self.the_data is None or not len(self.the_data) or self.the_data.empty:
            self._reload()
        return self.the_data

    @property
    def seconds(self):
        return self.the_seconds


if __name__ == '__main__':
    my_url = 'https://www.domain.com.au/sold-listings/nsw/'
    my_cache = RedataCache(my_url, page='3')
    my_data = my_cache.data
    print('>  len: {} bytes'.format(len(my_data)))
    print('> took: {:.2f} seconds'.format(my_cache.seconds))
