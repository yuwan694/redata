#!/usr/bin/env python

from redata.common_dot_com import CommonDotCom


class RealestateDotCom(CommonDotCom):
    def _build_suburb_url(self, a_dict):
        my_url = '{},+{}+{:04d}'.format(a_dict['locality'], a_dict['state'], a_dict['postcode'])
        my_url = my_url.replace(' ', '+')
        self.the_suburb_url = my_url.lower()

    def sold_listings(self):
        my_url = 'https://www.realestate.com.au/sold/in-{}/list-'.format(self.the_suburb_url)
        my_params = {}
        my_params.update(self.the_kwargs)
        # my_top_path = ['pageData', 'data', 'searchResults', 'tieredResults']  # , 0, 'results']
        my_top_path = ['pageData', 'data', 'tieredResults']
        my_info_locator = {
            'url': ['_links', 'prettyUrl', 'href'],
            'price': ['price', 'display'],
            'agent': ['lister', 'name'],
            'brand': ['agency', 'name'],
            'street': ['address', 'streetAddress'],
            'suburb': ['address', 'suburb'],
            'state': ['address', 'state'],
            'postcode': ['address', 'postcode'],
            'beds': ['generalFeatures', 'bedrooms', 'value'],
            'baths': ['generalFeatures', 'bathrooms', 'value'],
            'parking': ['generalFeatures', 'parkingSpaces', 'value'],
            'propertyType': ['propertyTypeDisplay'],
            'landSize': [None],
            'date': ['dateSold', 'display'],
        }

        self._clear_data()
        for my_page in range(self.the_begin_page, self.the_end_page + 1):
            self._report_page_progress(my_page)
            my_json = self._read_json_data(my_url + '{}'.format(my_page),
                                           my_params,
                                           my_page,
                                           'REA.initialState',
                                           '</script>')
            my_tiered_results = self._browse_dict_nodes(my_json, my_top_path)
            if my_tiered_results:
                for my_index, my_tiered_result in enumerate(my_tiered_results):
                    if 'results' in my_tiered_result and len(my_tiered_result['results']) > 0:
                        my_items = my_tiered_result['results']
                        self._store_data(['listingId'], my_items, my_info_locator)
            my_next = self._browse_dict_nodes(my_json, ['pageData', 'data', '_links', 'next'])
            if not my_next:
                if self.the_verbose:
                    print('The next is empty in page [{}], Stop.'.format(my_page))
                break
        return self.the_data


if __name__ == '__main__':
    try:
        my_invalid = RealestateDotCom(suburb='non')
    except ValueError as e:
        print('Exception(ValueError):', e)
    try:
        my_invalid = RealestateDotCom(suburb='200')
    except ValueError as e:
        print('Exception(ValueError):', e)

    my_real = RealestateDotCom(suburb='cherrybrook+2126+nsw')
    my_data = my_real.sold_listings()
    for my_item in my_data:
        print('item:', my_item, my_data[my_item])
    print('Count:', len(my_data))

    my_df = my_real.df
    my_purified_df = my_real.get_purified_df(oldest_date='2000-01-02', highest_price='10000000')
