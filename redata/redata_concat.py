#!/usr/bin/env python

from pandas import concat
from redata.common_dot_com import CommonDotCom
from redata.domain_dot_com import DomainDotCom
from redata.realestate_dot_com import RealestateDotCom
from redata.realestate_view_dot_com import RealestateViewDotCom


class RedataConcat(CommonDotCom):
    def __init__(self, **kwargs):
        self.the_domain = None
        self.the_real = None
        self.the_review = None
        super().__init__(**kwargs)

    def update_config(self, **kwargs):
        super().update_config(**kwargs)
        self.the_domain = DomainDotCom(**self.the_kwargs)
        self.the_real = RealestateDotCom(**self.the_kwargs)
        self.the_review = RealestateViewDotCom(**self.the_kwargs)

    def _build_suburb_url(self, a_dict):
        pass  # not used, but have to be implemented

    def sold_listings(self):
        self._clear_data()
        print('Processing DomainDotCom ...')
        my_domain = self.the_domain.sold_listings()
        print('Processing RealestateDotCom ...')
        my_real = self.the_real.sold_listings()
        print('Processing RealestateViewDotCom ...')
        my_review = self.the_review.sold_listings()

        self.the_data.update({int(my_key) + 10000000000: my_domain[my_key] for my_key in my_domain})
        self.the_data.update({int(my_key) + 20000000000: my_real[my_key] for my_key in my_real})
        self.the_data.update({int(my_key) + 30000000000: my_review[my_key] for my_key in my_review})
        return self.the_data

    def get_purified_df(self, **kwargs):
        my_domain = self.the_domain.get_purified_df(**kwargs)
        my_real = self.the_real.get_purified_df(**kwargs)
        my_review = self.the_review.get_purified_df(**kwargs)
        my_df = concat([my_real, my_domain, my_review],  # my_real first as they are better
                       axis=0,
                       sort=True)
        print('After concatenated:', my_df.shape)
        my_duplicated_cols = my_df.loc[:, ['price', 'street', 'beds', 'baths', 'parking']].duplicated()
        my_df = my_df[~my_duplicated_cols]
        my_df = my_df.sort_index()
        print('After deleting duplicates:', my_df.shape)
        return my_df


if __name__ == '__main__':
    my_concat = RedataConcat(suburb='cherrybrook+2126+nsw',
                             verbose=True,
                             oldest_date='2000-01-01',
                             highest_price=10000000)
    my_concat.sold_listings()
    my_df = my_concat.get_purified_df()
    print(my_df)
