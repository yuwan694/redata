#!/usr/bin/env python

import unittest
import requests


class RequestsTest(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    @unittest.skipIf(True, 'This is not actually unittest, but practicing.')
    def test_download_sold(self):
        my_suburbs = ['nsw', 'maryland-nsw-2287', 'ryde-nsw-2112']
        my_pages = ['1', '2', '3', '51']
        for my_suburb in my_suburbs:
            for my_page in my_pages:
                my_request = requests.get('https://www.domain.com.au/sold-listings/{}/'.format(my_suburb),
                                          {'page': my_page})
                my_content = my_request.content.decode('utf-8')
                my_begin = my_content.find('window.renderizrData["page"]')
                my_begin = my_content.find('{', my_begin)
                my_end = my_content.find(';</script>', my_begin)
                my_json = my_content[my_begin:my_end]
                print('> begin={}, end={}, content:\n  '.format(my_begin, my_end),
                      my_json[:50], '...', my_json[-50:])
                with open('/tmp/domain.{}.{}.json'.format(my_suburb, my_page), 'wb') as f:
                    f.write(my_json.encode('utf-8'))


if __name__ == '__main__':
    unittest.main()
