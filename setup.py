#!/usr/bin/env python

from distutils.core import setup

setup(name='redata',
      version='0.1.0',
      description='Collect RE (Real Estate) data from the famous Australian real estate websites, '
                  + 'and present as a pandas.DataFrame for easier analysis.',
      author='Wan YU',
      url='https://gitlab.com/yuwan694/redata',
      packages=['redata'])
