#!/usr/bin/env python

import os

import matplotlib
if 'My style matplotlib':
    matplotlib.rcParams['figure.dpi'] = 100
    matplotlib.rcParams['savefig.dpi'] = 100
    if os.environ.get('FIG_FILE'):
        matplotlib.use('Agg')  # This avoids interacting with QT which we don't need here.

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import unittest


class Auction(object):
    def __init__(self, a_csv, a_verbose=False):
        self.the_csv = a_csv
        self.the_df = pd.DataFrame()
        self.the_verbose = a_verbose
        self.the_read_opts = {
            'index_col': 0,
            'parse_dates': True,
            'dayfirst': True,
            'converters': {
                'NSW_Clearance_Rate': self.percent2float,
                'Sydney_Clearance_Rate': self.percent2float,
            }
        }
        self.wide_view()

    @classmethod
    def percent2float(cls, an_input):
        try:
            return float(an_input.rstrip('%')) / 100.
        except ValueError:
            return np.nan

    @classmethod
    def get_csv_path(cls, a_csv):
        return os.path.dirname(os.path.realpath(__file__)) + '/' + a_csv

    @classmethod
    def wide_view(cls):
        pd.set_option('display.max_rows', None)
        pd.set_option('display.max_columns', None)

    def get_df(self):
        if self.the_df.empty:
            my_df = pd.read_csv(self.the_csv, **self.the_read_opts)
            if self.the_verbose:
                print(my_df.shape)
                print(my_df.head())
            self.the_df = my_df
        return self.the_df

    def plot_multi_lanes(self):
        fig, axes = plt.subplots(4, 1, figsize=(9, 12), sharex='col')
        fig.subplots_adjust(hspace=0.0, wspace=0.0)

        styles = ['b-', 'm.--', 'g-', 'c-', 'r.--']

        my_df = self.get_df()
        my_df.plot(ax=axes[0],
                   style=styles,
                   secondary_y=['NSW_Clearance_Rate', 'Sydney_Clearance_Rate'],
                   title='NSW/Sydney Auction Count & Clearance Rates')

        count_data = my_df[['NSW_Auction_Count', 'NSW_Non-auction_Count', 'Sydney_Auction_Count']]
        rate_data = my_df[['NSW_Clearance_Rate', 'Sydney_Clearance_Rate']]
        count_data.plot(ax=axes[1], style=[styles[i] for i in [0, 2, 3]])
        rate_data.plot(ax=axes[2], style=[styles[i] for i in [1, 4]])

        event_data = my_df[['NSW_Clearance_Rate', 'Events']]
        event_data.NSW_Clearance_Rate.plot(ax=axes[3], style=styles[1], label='Events')
        axes[3].legend()
        [axes[3].text(x, event_data.NSW_Clearance_Rate[x],
                      ' {}'.format(event_data.Events[x]), verticalalignment='center')
         for x in event_data[event_data.Events.notnull()].index]
        plt.show()

    def plot_all(self):
        my_df = self.get_df()
        my_fig, my_ax = plt.subplots(1, 1, figsize=(16, 9))
        my_df.plot(
            ax=my_ax,
            secondary_y=['NSW_Clearance_Rate', 'Sydney_Clearance_Rate'],
            title='NSW/Sydney Auction Count & Clearance Rates',
            legend=False,
        )
        my_ax.grid()
        plt.legend(loc='upper right')
        my_ax.legend(loc='upper left')
        plt.show()


class AuctionTest(unittest.TestCase):
    the_action = Auction(Auction.get_csv_path('Auction.csv'), False)

    @classmethod
    def setUp(self) -> None:
        pd.set_option('display.max.columns', 256)
        pd.set_option('display.width', 1024)

    def test_percent2float(self):
        print(Auction.percent2float('73.25%'))
        self.assertEqual(0.7325, Auction.percent2float('73.25%'))

    def test_get_df(self):
        my_df = self.the_action.get_df()
        print(my_df.shape)
        print(my_df.head(10))

    def test_plot_multi_lanes(self):
        my_df = self.the_action.get_df()
        print(my_df.head(10))
        self.the_action.plot_multi_lanes()

    def test_plot_all(self):
        self.the_action.plot_all()


if __name__ == '__main__':
    unittest.main()
