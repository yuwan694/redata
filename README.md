# redata

This is another spin-off project from `ogarpy`, and original document is [here](docs/004.redata.md).  
Collect RE (Real Estate) data from the famous Australian real estate websites,
and present as a pandas.DataFrame for easier analysis.

## Installing

```bash
user@host:~/work$ git clone https://gitlab.com/yuwan694/redata.git
user@host:~/work/redata$ make init
user@host:~/work/redata$ make unit-tests
user@host:~/work/redata$ make self-tests
user@host:~/work/redata$ make install
```

## Running Notebooks

```bash
user@host:~/work/redata$ make notebooks
```

## Screen-shots

* Config your Suburb and Features  
  ![config-suburb-and-features.png](imgs/config-suburb-and-features.png)

* If your suburb setting is not fully qualified, some suggestions will be appeared, choose one of them.  
  ![suburb-suggestions.png](imgs/suburb-suggestions.png)

* This module processing a lot of pages, 50 to 200 pages per suburb per site.  
  It will print out progressing like below, if you set verbose=True.  
  ![processing-pages.png](imgs/processing-pages.png)  
  All downloaded data will be cached, so that next run would be quicker.
  
* A sample chart  
  ![sample-chart.png](imgs/sample-chart.png)
  